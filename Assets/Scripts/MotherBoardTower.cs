﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherBoardTower : MonoBehaviour {

    public MotherBoard CurrentMotherBoard {
        get {
            return currentMotherBoard;
        }
    }

    [System.NonSerialized] public List<MotherBoard> motherBoards = new List<MotherBoard>();
    
    [SerializeField] MotherBoard motherBoard;
    [SerializeField] float downSpeed;
    [SerializeField] float spacing = 1.0f;
    [SerializeField] int bonusSpan = 2;
    [SerializeField] int bonusCount = 8;

    Vector3 currentPosition;
    MotherBoard currentMotherBoard;
    AudioSource audioSource;
    bool isPlay = true;

    void Awake() {
        currentPosition = transform.position;
        audioSource = GetComponent<AudioSource>();
        CreateNewBoard();
    }

    void Update() {
        if (isPlay) {
            transform.position = Vector3.Lerp(transform.position, currentPosition, downSpeed * Time.deltaTime);
        } else {
            MoveResult();
        }
    }

    public void CreateNewBoard() {
        MotherBoard board = Instantiate(motherBoard) as MotherBoard;
        motherBoards.Add(board);
        board.gameObject.SetActive(true);
        board.transform.SetParent(transform, false);
        Vector3 pos = new Vector3(0.0f, motherBoards.Count * spacing, 0.0f);
        board.transform.localPosition = pos;
        currentPosition = new Vector3(0, -motherBoards.Count * spacing, 0);
        currentMotherBoard = board;
        if(motherBoards.Count % bonusSpan == 0) {
            GameManager.Instance.AddRemaining(bonusCount);
        } else {
            //audioSource.Play();
        }
    }

    public void AddMemory(Memory memory) {
        currentMotherBoard.AddMemory(memory);
        if(currentMotherBoard.MemoryCount >= 8) {
            CreateNewBoard();
        }
    }

    public void SetResult() {
        isPlay = false;
    }

    void MoveResult() {
        transform.position = Vector3.Lerp(transform.position, new Vector3(6.0f, -2.0f, 0.0f), 5 * Time.deltaTime);
        int size = Mathf.Clamp(motherBoards.Count, 0, 10);
        size -= 4;
        if (size != 0) {
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one * 0.8f - Vector3.one * (size - 1) * 0.08f, 5 * Time.deltaTime);
        }
    }

}
