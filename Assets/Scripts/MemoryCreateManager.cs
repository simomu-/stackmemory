﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MemoryCreateManager : MonoBehaviour {

    [SerializeField] MotherBoardTower motherBoardTower;
    [SerializeField] Memory[] memories;

    Memory currentMemory;
    int level = 1;
    bool canPlay = true;

    void Start() {
        currentMemory = Instantiate(memories[Random.Range(0, level)]);
        SetMemoryPosition(currentMemory);
    }

    void Update() {

        SetLevel();

        TouchPhase touch = TouchPhase.Began;
        if(Input.touchCount != 0) {
            touch = Input.GetTouch(0).phase;
        }

       if ((Input.GetButtonDown("Fire1") || Input.GetMouseButtonDown(1) || touch == TouchPhase.Ended) && canPlay) {
            motherBoardTower.AddMemory(currentMemory);
            currentMemory = Instantiate(memories[Random.Range(0, level)]) as Memory;
            SetMemoryPosition(currentMemory);
        }
    }

    void SetLevel() {
        level = motherBoardTower.motherBoards.Count;
        if (level >= memories.Length) {
            level = memories.Length;
        }
    }

    void SetMemoryPosition(Memory memory) {
        memory.transform.position = new Vector3(-5f, 2f, -2.8f + motherBoardTower.CurrentMotherBoard.MemoryCount * 0.5f);
    }

    public void Stop() {
        canPlay = false;
    }

    public void SetResult() {
        Destroy(currentMemory.gameObject);
    }

}
