﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : SingletonMonoBehaviour<GameManager> {

    public int Capacity {
        get {
            return capacity;
        }
    }

    int capacity;
    int life = 4;

    public void AddCapacity(int capacity) {
        this.capacity += capacity;
         UIManager.Instance.AddCapacity(capacity);
    }

    public void FailMemory() {
        life--;
        if(life <= 0) {
            life = 0;
            FindObjectOfType<MemoryCreateManager>().Stop();
            UIManager.Instance.Finish();
            GameFinish();
        }
        UIManager.Instance.DecreaseCount(1);
    }

    public void AddRemaining(int count) {
        if(life<= 0) {
            return;
        }
        life += count;
        if(life >= 5) {
            life = 5;
        }
        UIManager.Instance.IncreaseCount(count);
    }

    void GameFinish() {
        StartCoroutine(GameFinishCoroutine());
    }

    IEnumerator GameFinishCoroutine() {
       yield return new WaitForSeconds(2.0f);
        UIManager.Instance.SetResult();
        FindObjectOfType<MemoryCreateManager>().SetResult();
        FindObjectOfType<MotherBoardTower>().SetResult();
    }

    public void ResetScore() {
        life = 4;
        capacity = 0;
    }
    
}
