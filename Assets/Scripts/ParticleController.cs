﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour {

    public static ParticleController Instance {
        get {
            return instance;
        }
    }

    static ParticleController instance;
    ParticleSystem particle;

    void Start() {
        instance = this;
        particle = GetComponent<ParticleSystem>();
        particle.Pause();
    }

    public void Emit(Vector3 position) {
        transform.position = position;
        particle.Play();
    }

}
