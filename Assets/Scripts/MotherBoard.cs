﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MotherBoard : MonoBehaviour {


    public int MemoryCount {
        get {
            return memories.Count;
        }
    }

    [SerializeField] float allowableError;
    [SerializeField] float spacing = 0.5f;

    List<Memory> memories = new List<Memory>();
    AudioSource audioSource;

    void Start() {
        audioSource = GetComponent<AudioSource>();
    }

    void Update() {
        //for(int i = 0; i < memories.Count; i++) {
        //    memories[i].transform.localPosition = new Vector3(0.0f , 0.1f , -2.8f + i * spacing);
        //}
    }

    public void AddMemory(Memory memory) {
        if(memories.Count >= 8) {
            return;
        }

        if(memory.transform.position.x < -allowableError) {
            memory.SetFailMove(Memory.FailType.Right);
            GameManager.Instance.FailMemory();
            audioSource.Play();
            return;
        }

        if (memory.transform.position.x > allowableError) {
            memory.SetFailMove(Memory.FailType.Left);
            GameManager.Instance.FailMemory();
            audioSource.Play();
            return;
        }

        memory.IsMove = false;
        memory.transform.SetParent(transform, false);
        memory.SetMotherBoard(new Vector3(0.0f, 0.1f, -2.8f + memories.Count * spacing));
        memories.Add(memory);
        GameManager.Instance.AddCapacity(memory.Capacity);
        //UIManager.Instance.AddCapacity(memory.Capacity);
    }

}
