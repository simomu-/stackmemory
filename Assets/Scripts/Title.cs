﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Title : MonoBehaviour {

    [SerializeField] RectTransform howToPlayPanel;

    bool isHowToPlay = false;

    void Start() {
        howToPlayPanel.transform.localScale = Vector3.zero;
        howToPlayPanel.gameObject.SetActive(false);
    }

    void Update() {
        if (isHowToPlay) {
            howToPlayPanel.transform.localScale = Vector3.Lerp(howToPlayPanel.transform.localScale, Vector3.one, 7.0f * Time.deltaTime);
        }

        TouchPhase touch = TouchPhase.Began;
        if (Input.touchCount != 0) {
            touch = Input.GetTouch(0).phase;
        }

        if (isHowToPlay && (Input.GetButtonDown("Fire1") || Input.GetMouseButtonDown(1) || touch == TouchPhase.Ended)) {
            SceneManager.LoadScene("Game");
        }
    }

    public void HowToPlay() {
        isHowToPlay = true;
        howToPlayPanel.gameObject.SetActive(true);
    }

}
