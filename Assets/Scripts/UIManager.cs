﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

    public static UIManager Instance {
        get {
            return instance;
        }
    }

    static UIManager instance;

    [SerializeField] RectTransform gameUIPanel;
    [SerializeField] Text currentCapacityText;
    [SerializeField] Text addCapacityText;
    [SerializeField] Text failMemoryCountText;
    [SerializeField] Text changeFailCountText;
    [SerializeField] Text finishText;
    [Space]
    [SerializeField] RectTransform resultUIPanel;
    [SerializeField] Text resultText;

    Animator capacityAnimator;
    Animator countAnimator;
    int currentCapacity = 0;
    int failMemoryCount = 4;
    Vector3 textSize;
    AudioSource audioSource;

    void Start() {
        instance = this;
        capacityAnimator = addCapacityText.gameObject.GetComponent<Animator>();
        countAnimator = changeFailCountText.gameObject.GetComponent<Animator>();
        audioSource = GetComponent<AudioSource>();
    }

    void FixedUpdate() {
        if(currentCapacity - int.Parse(currentCapacityText.text) != 0) {
            currentCapacityText.text = (int.Parse(currentCapacityText.text) + 1).ToString();
        }
        currentCapacityText.transform.parent.localScale = Vector3.Lerp(currentCapacityText.transform.parent.localScale, Vector3.one, Time.deltaTime * 10.0f);

        failMemoryCountText.transform.localScale = Vector2.Lerp(failMemoryCountText.transform.localScale, Vector3.one, Time.deltaTime * 10.0f);

        if(int.Parse(failMemoryCountText.text) < failMemoryCount) {
            failMemoryCountText.text = (int.Parse(failMemoryCountText.text) + 1).ToString();
        }
        if(int.Parse(failMemoryCountText.text) > failMemoryCount) {
            failMemoryCountText.text = (int.Parse(failMemoryCountText.text) - 1).ToString();
        }

        if (int.Parse(failMemoryCountText.text) <= 2) {
            failMemoryCountText.color = Color.red;
        }else {
            failMemoryCountText.color = Color.white;
        }
    }

    public void AddCapacity(int capacity) {
        addCapacityText.text = "+" + capacity.ToString() + "GB";
        currentCapacity += capacity;
        capacityAnimator.SetTrigger("Add");
        currentCapacityText.transform.parent.localScale = Vector3.one * 1.2f;
    }

    public void FailMemory(int count) {
        failMemoryCountText.text = count.ToString();
        failMemoryCountText.transform.localScale = Vector3.one * 1.2f;
    }

    public void IncreaseCount(int count) {
        failMemoryCount += count;

        int c = count;
        if (failMemoryCount > 5) {
            c = failMemoryCount - 5;
            failMemoryCount = 5;
        }

        changeFailCountText.text = "+" + c.ToString();
        countAnimator.SetTrigger("Add");
        failMemoryCountText.transform.localScale = Vector3.one * 1.2f;
        audioSource.Play();
    }

    public void DecreaseCount(int count) {
        failMemoryCount -= count;
        if(failMemoryCount <= 0) {
            failMemoryCount = 0;
        }
        changeFailCountText.text = "-" + count.ToString();
        countAnimator.SetTrigger("Add");
        failMemoryCountText.transform.localScale = Vector3.one * 1.2f;
    }

    public void Finish() {
        finishText.gameObject.SetActive(true);
    }

    public void SetResult() {
        finishText.gameObject.SetActive(false);
        gameUIPanel.gameObject.SetActive(false);
        resultUIPanel.gameObject.SetActive(true);
        resultText.text = string.Format("あなたはコンピュータに\n{0}GB\nのメモリを積みました", GameManager.Instance.Capacity);
    }

    public void SendRanking() {
        naichilab.RankingLoader.Instance.SendScoreAndShowRanking(GameManager.Instance.Capacity);
    }

    public void TweetResult() {
        string text = string.Format("{0}GB のメモリを積みました #unity1week #unityroom #StackMemory\n", GameManager.Instance.Capacity);
        text += "https://unityroom.com/games/stack_memory";
        string url = "http://twitter.com/intent/tweet?text=" + WWW.EscapeURL(text);
        if (Application.platform == RuntimePlatform.WebGLPlayer) {
            Application.ExternalEval("window.open(\"" + url + "\",\"_blank\")");
            return;
        }
        Application.OpenURL(url);
    }

    public void GotoTitle() {
        UnityEngine.SceneManagement.SceneManager.LoadScene("Title");
        GameManager.Instance.ResetScore();
    }
}
