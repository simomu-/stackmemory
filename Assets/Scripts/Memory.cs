﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Memory : MonoBehaviour {

    public enum FailType {
        Right,
        Left
    }

    [System.NonSerialized] public bool IsMove = true;
    public int Capacity {
        get {
            return memoryCapacity;
        }
    }

    [SerializeField] int memoryCapacity;
    [SerializeField] float speed;
    [SerializeField] float moveDistance = 1.0f;

    Vector3 startPosition;
    Vector3 positionOnBoard;
    float currentTime = 0;
    bool isIncrement = true;
    bool isMoveToBoard = false;
    bool isFailMove = false;
    FailType failType;
    AudioSource audioSource;

    // Use this for initialization
    void Start() {
        startPosition = transform.position;
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update() {
        if (IsMove) {
            Move();
        }

        if (isMoveToBoard) {
            MoveToBoard();
        }

        if (isFailMove) {
            FailMove();
        }
    }

    void Move() {
        if (isIncrement) {
            currentTime += Time.deltaTime * speed;
            if (currentTime >= 1.0f) {
                isIncrement = false;
            }
        } else {
            currentTime -= Time.deltaTime * speed;
            if (currentTime <= 0) {
                isIncrement = true;
            }
        }
        transform.position = startPosition + new Vector3(CubicInOut(currentTime) * moveDistance, 0.0f, 0.0f);
    }

    void MoveToBoard() {
        transform.localPosition = Vector3.Lerp(transform.localPosition, positionOnBoard, 10.0f * Time.deltaTime);
        if (Vector3.Distance(transform.localPosition, positionOnBoard) <= 0.05f) {
            isMoveToBoard = false;
            audioSource.Play();
        }
    }

    void FailMove() {

        float dx = (failType==FailType.Right) ? -20f : 20f;
        float rz = (failType == FailType.Right) ? 720f : -720f;

        currentTime += Time.deltaTime;
        Vector3 pos = transform.position;
        pos.x = pos.x + dx * Time.deltaTime;
        pos.y = (1 - BounceOut(currentTime * 2)) * 2;
        transform.position = pos;
        transform.Rotate(0.0f, 0.0f, rz * Time.deltaTime);
    }

    float CubicInOut(float time) {
        float t = Mathf.Clamp01(time);
        if ((t /= 0.5f) < 1) {
            return 0.5f * t * t * t;
        }
        return 0.5f * ((t -= 2) * t * t + 2);
    }

    float BounceOut(float time) {
        float t = Mathf.Clamp01(time);
        if ( t < (1 / 2.75f)) {
            return (7.5625f * t * t);
        } else if (t < (2 / 2.75f)) {
            float postFix = t -= (1.5f / 2.75f);
            return (7.5625f * (postFix) * t + .75f);
        } else if (t < (2.5 / 2.75)) {
            float postFix = t -= (2.25f / 2.75f);
            return (7.5625f * (postFix) * t + .9375f);
        } else {
            float postFix = t -= (2.625f / 2.75f);
            return (7.5625f * (postFix) * t + .984375f);
        }
    }

    public void SetMotherBoard(Vector3 position) {
        positionOnBoard = position;
        IsMove = false;
        isMoveToBoard = true;
        transform.localPosition = new Vector3(position.x, 2.0f, position.z);
    }

    public void SetFailMove(FailType type) {
        IsMove = false;
        isMoveToBoard = false;
        isFailMove = true;
        currentTime = 0;
        failType = type;
        Destroy(gameObject, 1f);
    }

}
