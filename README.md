# README #

### 概要 ###

2017/06/19~2017/06/25に開催されたUnity 1週間ゲームジャム お題「積む」の作品

「え？メモリ16GB？、俺のPCメモリ512GBあるけど？」 
タイミングよくクリックしてメモリをたくさん積んで 
最強のコンピュータを作ろう！

### 完成品 ###

https://unityroom.com/games/stack_memory

### 依存アセット ###

以下の外部アセットが必要です

Standard AssetsのCrossPlatformInput

Post Processing Stack
https://www.assetstore.unity3d.com/jp/#!/content/83912

naichilab様のunity-simple-ranking
https://github.com/naichilab/unity-simple-ranking


